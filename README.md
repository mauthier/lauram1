# Estimation des temps de génération et des seuils de déclin de cétacés en tenant compte de la phylogénie et de la masse 

#### Stage de Master 1 de Laura Clain
#### Encadrants: Benoît Simon-Bouhet et Matthieu Authier


### Résumé
La Commission de conservation européenne OSPAR a défini un indicateur commun nommé M4 qui permet de suivre les variations d’abondance de cétacés. Il est basé sur un seuil de déclin (soit 30% sur 3 générations) à ne pas dépasser pour conserver le bon état écologique de la population. Les estimations de temps de génération et de seuils de Taylor et al. (2007) basés sur 5 paramètres démographiques sont utilisés pour le M4. Une nouvelle méthode développée par Cooke et al. (2018) permet d’estimer les temps de génération tenant compte de la phylogénie et de la masse (modèle PM). Basée sur cette méthode, cette présente étude estime les temps de génération et les seuils de déclin sur 1 an de 77 espèces de cétacés dont 14 espèces d’intérêt vivant en Atlantique Nord Est. 53.10 % de l’information est portée par les données de masse et 46.9 % par les données phylogénétiques. Bien que les estimations des seuils de déclin puissent varier jusqu’à 0.3% (P.phocoena) entre le modèle PM et le modèle de Taylor et al. (2007), elles représentent plus de 40% de variation pour 3 espèces (S. Frontalis ; B. physalus et O.orca). Des erreurs de prédictions pourraient venir des sources des données car celles-ci sont définies au niveau de l’espèce, ne prenant pas en compte les différents écotypes existants. Par ailleurs ces différences ne seraient pas interprétables car de si petites différences de seuils seraient indétectables. Ainsi, les seuils estimés par Taylor et al. (2007) peuvent être définis comme robustes. 



### Sources des données 

Données phylogénétiques :
issues de McGowen et al., 2020. données disponibles sur https://doi.org/10.5061/dryad.jq40b0f : fichier RAxML_partition_by_gene_best_tree.tre
McGowen, M. R., Tsagkogeorga, G., Álvarez-Carretero, S., Dos Reis, M., Struebig, M., Deaville, R., Jepson, P. D., Jarman, 
S., Polanowski, A., Morin, P. A., & Rossiter, S. J. (2020). Phylogenomic Resolution of the Cetacean Tree of Life Using Target Sequence Capture. 
Systematic Biology, 69(3), 479–501. https://doi.org/10.1093/sysbio/syz068

Données d'histoire de vie :
issues de Pacifici, M., Santini, L., Di Marco, M., Baisero, D., Francucci, L., Marasini, G. G., Visconti, P., & Rondinini, C. (2013). Generation length for mammals. Nature Conservation, 5, 87–94. https://doi.org/10.3897/natureconservation.5.5734
(les cétacés ont été sélectionnés parmis toutes les données de mammifères)



### Méthodes utilisées

Méthode de calcul du temps de génération :
issues de Cooke RSC, Gilbert TC, Riordan P, Mallon D (2018) 
Improving generation length estimates for the IUCN Red List. PLoS ONE 13(1): e0191770. https://doi.org/10.1371/journal.pone.0191770
Script utilisé disponible sur https://doi.org/10.1371/journal.pone.0191770.s006

Méthode d'estimation du paramètre $z$ :
Pacifici, M., Santini, L., Di Marco, M., Baisero, D., Francucci, L., Marasini, G. G., Visconti, P., & Rondinini, C. (2013). Generation length for mammals. Nature Conservation, 5, 87–94. https://doi.org/10.3897/natureconservation.5.5734

Parameter $z$ is the fraction of adult life span that passes before the average individual has contributed half of its lifetime, age-weighted, reproduction.

#### infos packages

MPSEM : https://cran.r-project.org/web/packages/MPSEM/MPSEM.pdf

relaimpo : https://cran.r-project.org/web/packages/relaimpo/relaimpo.pdf

M4threshold : Matthieu Authier. 2021



