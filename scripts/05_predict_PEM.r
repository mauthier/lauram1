predict_PEM <- function(object,
                        targets,
                        lmobject,
                        newdata = NULL,
                        interval = c("none", "confidence", "prediction"),
                        level = 0.95
                        ){
  require(mvtnorm)
  if(is.null(newdata)) {
    stop("must provide a dataframe from which to predict")
  }
  tmp <- Locations2PEMscores(object, targets)
  if(nrow(newdata) != nrow(tmp$scores)) {
    stop("mismatch in newdata: number of rows differents than that in object and targets")
  }
  newdata <- cbind(newdata, tmp$scores)

  Residual.variance <- diag(t(lmobject$residuals) %*% lmobject$residuals)/lmobject$df
  Xh <- cbind(1,
              as.matrix(newdata[, attr(lmobject$terms, "term.labels"), drop = FALSE])
  )
  pred <- (matrix(rep(lmobject$coefficients, 1e3), nrow = 1e3, ncol = ncol(vcov(lmobject)), byrow = TRUE) +
             mvtnorm::rmvt(1e3, sigma = vcov(lmobject), df = lmobject$df)) %*%
    t(Xh)

  if (interval == "none") {
    out <- data.frame(sp = row.names(tmp$scores),
                      values = drop(Xh %*% lmobject$coefficients)
    )
  }
  if (interval == "confidence") {
    out <- data.frame(sp = row.names(tmp$scores),
                      values = drop(Xh %*% lmobject$coefficients),
                      lower = apply(pred, 2, quantile, probs = 0.5 * (1 - level)),
                      upper = apply(pred, 2, quantile, probs = 1 - 0.5 * (1 - level))
    )
  }
  if (interval == "prediction") {
    pred <- apply(pred, 2, function(col_j) { col_j + sqrt(Residual.variance) * rnorm(length(col_j)) })
    out <- data.frame(sp = row.names(tmp$scores),
                      values = drop(Xh %*% lmobject$coefficients),
                      lower = apply(pred, 2, quantile, probs = 0.5 * (1 - level)),
                      upper = apply(pred, 2, quantile, probs = 1 - 0.5 * (1 - level))
    )
  }
  return(out)
}
